"""
# Given an active window, take a screenshot of it and
# check whether an 'x' button is present.
# If there is one, submit a left click on it.
# Such targets are defined under resources/targets/
"""

import os, glob, cv2, time
import win32api, win32con
import numpy as np

from screenshot import take_screenshot
from colorama import init, Fore

## convert stdio for Windows
init(convert = True)

## config
SLEEP_TIME = 2 ## seconds
PROCESS_TITLE = "Droid4X 0.10.6 Beta"
OUTPUT = "output/output.png"

def main():
    window_rect = take_screenshot(PROCESS_TITLE)
    if (window_rect):
        draw_coordinates(window_rect)
    else:
        print(Fore.RED + "Error: '%s' not found." % PROCESS_TITLE)

def draw_coordinates(window_rect):
    IMAGE_RGB = cv2.imread('resources/entries/entry.png')
    IMAGE_GRAY = cv2.cvtColor(IMAGE_RGB, cv2.COLOR_BGR2GRAY)
    TARGETS = glob.glob('resources/targets/*.png')

    for target in TARGETS:
        template = cv2.imread(target, 0)
        template_width, template_height = template.shape[::-1]

        match = cv2.matchTemplate(IMAGE_GRAY, template, cv2.TM_CCOEFF_NORMED)
        threshold = 0.9
        result = np.where(match >= threshold)

        for coordinates in zip(*result[::-1]):
            cv2.rectangle(
                IMAGE_RGB,
                coordinates,
                (coordinates[0] + template_width, coordinates[1] + template_height),
                (0, 0, 255),
                2
            )

            ## click @coordinates
            x = window_rect[0] + (coordinates[0] + template_width / 2)
            y = window_rect[1] + (coordinates[1] + template_height / 2)
            print(x, y)
            click(x, y)
            time.sleep(SLEEP_TIME)
        
    cv2.imwrite(OUTPUT, IMAGE_RGB)
    print(Fore.GREEN + "Success: %s/%s" % (os.getcwd(), OUTPUT))

def click(x, y):
    win32api.SetCursorPos((x, y))
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, x, y, 0, 0)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, x, y, 0, 0)

if __name__ == '__main__':
    main()
