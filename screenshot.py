import pyscreenshot as ImageGrab
import win32gui
import sys

def take_screenshot(PROCESS_TITLE):
    print("Looking for a window with title -> %s..." % PROCESS_TITLE)

    hwnd = win32gui.FindWindow(None, PROCESS_TITLE)
    if (hwnd == 0):
        return False

    x, y, w, h = get_window_react(hwnd)

    # bring window on top
    win32gui.ShowWindow(hwnd, 5)
    win32gui.SetForegroundWindow(hwnd)

    # grab w/ given window coordinates
    im = ImageGrab.grab(bbox=(x, y, x + w, y + h)) # x1, y1, x2, y2

    # save image file
    im.save('resources/entries/entry.png')
    return x, y, w, h

def get_window_react(hwnd):
    rect = win32gui.GetWindowRect(hwnd)
    x = rect[0]
    y = rect[1]
    w = rect[2] - x
    h = rect[3] - y
    return x, y, w, h
